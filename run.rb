#!/bin/env ruby
# A trial of throwing ideas into code
code = <<~eĥo
  test with #0 multiple words
  test with #0 multiple lines
eĥo

# if true, this is not a comment so emit something
$say = true

# Toggle the $say value, that is permut the emmsion of code for the currently
# parsed pan
def bolt
  $say = !$say
  true # yes, interupt current sag call after any call
end

$lexic = {
  '#0': :bolt, # the default keyword to bolt
  '#10':

}

# Form a lexie from a given lexem and grammatical context
# Prepare for further processing
# lex: The lexie to consider.
# lie: The terrain and conditions surrounding the lex.
def sag(lex, lie=nil)
  if $lexic.key?(lex)
    return if send($lexic[lex])
  end
  "lexie: #{lex}" if $say
end

# Interpret the given eĥo code
def ret(code)
  code.split.map{|lexie| sag(lexie.to_sym)}.compact.join("\n")
end

puts ret(code)
