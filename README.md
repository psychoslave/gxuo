# Ĝuo, la transkutima programlingvo 

Celas provizi:

- rekomandota tutvoĉebla ĉefversio
- donota iloj por transformi plenkodbazo al aliaj vortprovizoj

# Loza idearo

- la projekto uzas Esperanto kiel ĉeflaborlingvo, sed mezterme celo estas ajnlingve verkebli
- *ĉiu* estas objekto, eĉ ŝaltpelilo kiel `se`, `dum`…
- en la bazversio 
  - uzi "-" kiel vortkunigo, anstataŭ "." 
  - neniu signo ekster minusklaj esperantaj leteroj kaj la streko `-`
  - alisignaro estas forigota ekster tio uzota en ĉenoj
  - ĉenoj estas kreotaj per klasfuncio, ne per specialaj signoj kiel `"`
- enigado de eroj en klasoj estas plenparto de la lingvo: `klasnomo-arigu bloko`
- ĉiuj objektoj idas el la reflekta klaso "eĥo" ĉar:
  - ĝi elvokas ke kodo ĉiam estas fora ehô de homa menso
  - tiu klaso havas povon por produkti mem-derivadon.